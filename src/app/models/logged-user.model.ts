export interface LoggedUser{
  userId:string;
  userDisplayName: string;
  role:string;
  grants:[string];
  tenantName:string;
  tenantCode:string;
  token: string;
  refreshToken:string;
}
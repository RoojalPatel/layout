import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../services/business/layout.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Breadcrumbservice } from '../services/utility/bread-crumb.service';
import { ObjectService } from '../services/business/object.service';

@Component({
  selector: 'app-advancesearch',
  templateUrl: './advancesearch.component.html',
  styleUrls: ['./advancesearch.component.css']
})
export class AdvancesearchComponent implements OnInit {
  advanceSearchLayout;
  objectId;
  object;
  constructor(
    private route: ActivatedRoute,
    private layoutServ:LayoutService,
    private router:Router,
    private objectService:ObjectService,
    private breadCrumbServ:Breadcrumbservice
  ) { 

  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.objectId = params.objectID;
      this.objectService.getObjectById(this.objectId).subscribe(
        data=>{
          this.object = data;
          this.breadCrumbServ.breadCrumbDataModel = [
            {label: this.object.name, routerLink:['/home/objects', this.objectId]},
            {label: "Adance Search"}
          ];
        }
      );
      this.layoutServ.getAdvancedSearchLayout(this.objectId).subscribe(
        data=>{
          this.advanceSearchLayout = data[0];
          if(this.advanceSearchLayout !=undefined){
            this.advanceSearchLayout.mainSection[0].columns.forEach(column => {
              column.columnData.forEach(element=>{
                element.fieldLovs = [];
                this.formatLayoutFields(element);
                this.populateFieldElementType(element);
              });
            });
          }
          
        }
      );
    });
    
  }

  formatLayoutFields(field){
    this.layoutServ.getFieldById(field.fieldId).subscribe(
      data=>{
        let tempData=data[0];
        field.fieldName = tempData.field_name;
        field.fieldType = tempData.field_type;
        
        field.fieldLovs = tempData.field_lov!=undefined?tempData.field_lov:[];
      }
    );
  }

  populateFieldElementType(data){
    switch(data.fieldType){
      case "String":
        data.fieldElementType = "text";
        break;
      case "Number":
        data.fieldElementType = "number";
        break;
      case "Boolean":
        data.fieldElementType = "radio";
        break;
      case "Date":
        data.fieldElementType = "date";
        break;

    }

    if(data.fieldLovs.length>0){
      data.fieldElementType = "dropdown";
    }
  }

}

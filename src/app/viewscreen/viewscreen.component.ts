import { ObjectService } from './../services/business/object.service';
import { ScreensService } from './../services/business/screens.service';
import { Router } from '@angular/router';
import { LayoutService } from './../services/business/layout.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Breadcrumbservice } from '../services/utility/bread-crumb.service';

@Component({
  selector: 'app-viewscreen',
  templateUrl: './viewscreen.component.html',
  styleUrls: ['./viewscreen.component.css']
})
export class ViewscreenComponent implements OnInit {
  objectId;
  screenId;
  object;
  screen = null;
  layout = null;
  rows;
  columns;
  data;
  breadCrumb;
  constructor(
    private route: ActivatedRoute,
    private layoutServ:LayoutService,
    private router:Router,
    private location:Location,
    private screenServ:ScreensService,
    private objectService:ObjectService,
    private breadCrumbServ:Breadcrumbservice
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.objectId = params.objectID;
      this.screenId = params.screenId;
      this.objectService.getObjectById(this.objectId).subscribe(
        data=>{
          this.object = data;
          this.breadCrumbServ.breadCrumbDataModel = [
            {label: this.object.name, routerLink:['/home/objects', this.objectId]},
            {label: "View"}
          ];
        }
      );
      this.screenServ.getScreenById(this.screenId).subscribe(
        data=>{
          this.screen = data;
          this.doDataPopulation();    
        }
      );
    });
  }  

  deleteScreen(screen){
    this.screenServ.deleteScreen(screen.id).subscribe(
      data=>{
        this.screen=null;
        this.layout=null;
        this.location.back();
    });
  }

  doDataPopulation(){
    this.layoutServ.getAllFields().subscribe(
      fields=>{
        let allFields = fields;
        this.layoutServ.getLayoutById(this.screen.layoutId).subscribe(
          data=>{
            let tempData = data;
            
            tempData.mainSection.forEach(section=>{
              section.columns.forEach(column=>{
                column.columnData.forEach(fieldData=>{
                  let originalField = allFields.filter((fieldElement)=>{
                    return fieldElement.id == fieldData.fieldId;
                  })[0];
                  fieldData.fieldName   = originalField.field_name;
                  let screenData = this.screen.data.filter(element=>{
                    return fieldData.fieldId == element.fieldId;
                  })[0];
                  fieldData.fieldValue = screenData.fieldValue;
                });
                if(column.relatedItems.length >0){
                  column.relatedItems.forEach(relatedItem => {
                    this.screenServ.getRelatedItemsvalues(relatedItem.objectId).subscribe(
                      data=>{
                        let relatedItemsValues = data[0];
                        this.rows = relatedItemsValues.data.fieldValues;
                        this.columns = Object.keys(this.rows[0]);
                        this.data = this.rows;
                      }
                    );
                  });
                }
                
              });
            });
            this.layout = tempData;
        });
    });
  }

}

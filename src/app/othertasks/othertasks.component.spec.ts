import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OthertasksComponent } from './othertasks.component';

describe('OthertasksComponent', () => {
  let component: OthertasksComponent;
  let fixture: ComponentFixture<OthertasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OthertasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OthertasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

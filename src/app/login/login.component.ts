import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './../services/business/login.service';
import { tokenNotExpired } from 'angular2-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form:FormGroup;
  tenantNameControl:FormControl = new FormControl("", [Validators.required]);
  userIdControl:FormControl = new FormControl("", [Validators.required]);
  paswordControl:FormControl = new FormControl("", [Validators.required]);

  constructor(
    private router:Router,
    private loginServ:LoginService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      "tenantCode":this.tenantNameControl,
      "userName": this.userIdControl,
      "password": this.paswordControl
    });
    if(this.loginServ.loginWithToken())
      this.router.navigate(["home/dashboard"]);
  }

  doLogin(){
    this.loginServ.doLogin(this.userIdControl.value, this.paswordControl.value, this.tenantNameControl.value);
  }

}

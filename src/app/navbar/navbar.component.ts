import { ObjectService } from './../services/business/object.service';
import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { SlideInOutNoHeightAnimation } from './../app.animations';
import { Breadcrumbservice } from '../services/utility/bread-crumb.service';
import { LoginService } from './../services/business/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  animations: SlideInOutNoHeightAnimation,
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  items: MenuItem[];
  visibility;
  objects;
  aDetailsVisibleState = false;
  objectMenuItems;
  objectId = 2;
  adminSubmenu = false;

  @Input("hideMenu") hideMenu;

  constructor(
      private objectService:ObjectService,
      private breadCrumb:Breadcrumbservice,
      private loginServ:LoginService,
      private router:Router
  ){

  }
  
  ngOnInit() {
      this.objectService.getAllObjects().subscribe(
          data=>{
              this.objects = data;
          }
      );
      // this.breadCrumb = [
      //   {label:'Contacts', routerLink:['/home/objects', this.objectId]},
      //   {label:'View Contacts'}
      // ];
      this.objectMenuItems = [
        {
          label:'Layouts', 
          items: [
            {
              label: 'New',
              icon: 'fa-plus',
              routerLink: ['/home/create-layout', this.objectId]
            },
            {
              label:'List All',
              icon: 'fa-folder-open',
              routerLink: ['/home/page-layouts', this.objectId]
            }
          ]
        },
        {
          label:'Fields', 
          items: [
            {
              label: 'New',
              icon: 'fa-plus',
              //routerLink: ['/create-layout', this.objectId]
            },
            {
              label:'List All',
              icon: 'fa-folder-open',
              //routerLink: ['/page-layouts', this.objectId]
            }
          ]
        },
      ];
  }

  sidenavClose(id){
      this.objects.forEach(element => {
          if(element.id == id)
            element.clicked = true;
          else
            element.clicked = false;
      });
      this.visibility = false;
  }

  logOut(){
    this.loginServ.doLogout();
    this.router.navigate(['/login']);
  }

}

export class DragNDropDataModel{
    public layoutName;
    public object_id;
    public layout_type;
    public mainSection:SubSection[];
}

export class SubSection{
    public sectionName;
    public sectionContent;
    public sectionId;
    public showEdit = false;
    public columns:[Columns];
}

export class Columns{
    public columnId;
    public highlight = false;
    public columnData = [];
    public relatedItems = [];
}

export class RelatedItemsObject{
    public objectId;
    public objectName;
    public data = [];
}

export class Data{
    public showProperties=false;
    public fieldId;
    public fieldName;
    public fieldType;
    public fieldValue;
    public fieldLength;
    public fieldLovs;
    public fieldElementType;
    public description;
    public mandatory = false;
    public readOnly = false;

    constructor(fieldName, fieldType){
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }
}

import { ObjectService } from './../services/business/object.service';
import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { DragNDropDataModel, RelatedItemsObject, SubSection, Columns, Data } from "./model/dragndropdata.model"
import {SortablejsOptions} from 'angular-sortablejs';
import { LayoutService } from '../services/business/layout.service';
import {ActivatedRoute, Router} from "@angular/router";
import {SelectItem} from 'primeng/api';
import { Breadcrumbservice } from '../services/utility/bread-crumb.service';

@Component({
  selector: 'app-createlayout',
  templateUrl: './createlayout.component.html',
  styleUrls: ['./createlayout.component.css']
})
export class CreatelayoutComponent implements OnInit {
  public mainLayout = new DragNDropDataModel;
  public options:SortablejsOptions;
  public options1:SortablejsOptions;
  public relatedItemsDragged = true;
  public fieldsDragged = true;
  public firstBag = [];
  public relatedItems = [];
  public dragover;
  public section;
  public column;
  public objectId;
  public layoutId;
  sectionDialog = false;
  fieldSettingDialog = false;
  saveLayoutDialog = false;
  layoutTypeDialog = false;
  sectionContentType:SelectItem[]= [
    {label:"Fields", value:"fields"},
    {label:"Related Items", value:"related_items"}
  ];
  layoutTypes:SelectItem[] = [
    {label:"Basic Search", value:"basic_search"},
    {label:"Advance Search", value:"advanced_search"},
    {label:"Search Result", value:"search_result"},
    {label:"Records", value:"object_record"}
  ];
  sectionData = {
    sectionName:"",
    sectionColumns:2,
    sectionContent:"fields"
  };
  fieldSetting = {
    value:100
  };

  layoutName = "";

  selectedField;
  selectedSection;
  layoutFor;


  constructor(
    private location:Location,
    private route: ActivatedRoute,
    private router:Router,
    public layoutService:LayoutService,
    private objectService:ObjectService,
    private breadCrumbServ:Breadcrumbservice
  ) { 

  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.objectId = params.objectID;
      this.objectService.getObjectById(this.objectId).subscribe(
        data=>{
          let obj = data;
          this.breadCrumbServ.breadCrumbDataModel = [
            {label:obj.name, routerLink:['/home/objects', obj.id]},
            {label:'Layouts', routerLink:['/home/page-layouts', obj.id]}
          ];
          if(params.layoutId != undefined){
            this.breadCrumbServ.breadCrumbDataModel.push({label:'Edit Layout'});
          }
          else{
            this.breadCrumbServ.breadCrumbDataModel.push({label:'create Layout'});
          }
        }
      );
      this.layoutService.getAllFields().subscribe(
        data=>{
          let tempData = data;
          tempData.forEach(element => {
            let fieldData = new Data(element.field_name, element.field_type);
            fieldData.fieldId = element.id;
            fieldData.description = element.description;
            this.firstBag.push(fieldData);
          });

          this.layoutService.getAllRelatedItems().subscribe(
            data=>{
              let tempData = data;
              tempData.forEach(element => {
                let relatedData = new RelatedItemsObject;
                relatedData.objectId = element.object_id;
                relatedData.objectName = element.object_name;
                element.fields.forEach(relatedField=>{
                  let fieldData = new Data(relatedField.field_name, relatedField.field_type);
                  fieldData.fieldId = relatedField.id;
                  relatedData.data.push(fieldData);
                });
                this.relatedItems.push(relatedData);
              });
            }
          );

          if(params.layoutId == undefined){
            this.layoutTypeDialog = true;
          }
          
          if(params.layoutId != undefined){
            this.layoutId = params.layoutId;
            this.layoutService.getLayoutById(this.layoutId).subscribe(
              data=>{
                let tempData = data;
                tempData.mainSection.forEach(section=>{
                  section.columns.forEach(column=>{
                    column.columnData.forEach(data=>{
                      let originalField = this.firstBag.filter((fieldElement)=>{
                        return fieldElement.fieldId == data.fieldId;
                      })[0];
                      data.fieldName = originalField.fieldName;
                      data.fieldType = originalField.fieldType;
                      this.firstBag = this.firstBag.filter(fieldElement=>{
                        return fieldElement.fieldId != data.fieldId;
                      });
                    });
                    
                    column.relatedItems.forEach(data=>{
                      let originalField = this.relatedItems.filter((fieldElement)=>{
                        return fieldElement.objectId == data.objectId;
                      })[0];
                      data.data = originalField.data;
                      this.relatedItems = this.relatedItems.filter(fieldElement=>{
                        return fieldElement.objectId != data.objectId;
                      });
                    });
                  });
                });
                this.mainLayout = tempData;
              }
            );
          }
        }
      );
    });
    this.options = {
      group: 'fields',
      onStart: (event:any)=>{
        this.fieldsDragged = true;
        this.relatedItemsDragged = false;
      },
      onEnd: (event:any)=>{
        this.fieldsDragged = true;
        this.relatedItemsDragged = true;
      }
    };
    this.options1 = {
      group: 'relatedItems',
      onStart: (event:any)=>{
        this.fieldsDragged = false;
        this.relatedItemsDragged = true;
      },
      onEnd: (event:any)=>{
        this.fieldsDragged = true;
        this.relatedItemsDragged = true;
      }
    };
  }

  onLayoutTypeSave(){
    this.mainLayout.layout_type = this.layoutFor;
    this.createSection(this.layoutFor);
    this.layoutTypeDialog = false;
  }

  deleteSection(section){
    section.columns.forEach(column => {
      column.columnData.forEach(data=>{
        this.firstBag.push(data);
      }) ;
      column.relatedItems.forEach(data=>{
        this.relatedItems.push(data);
      });
    });
    this.mainLayout.mainSection = this.mainLayout.mainSection.filter(element=>{
      return section.sectionName != element.sectionName;
    });
  }

  openFieldSettingModal(data){
    this.selectedField = data;
    this.fieldSetting = {
      value:(data.mandatory?1:(data.readOnly?2:100))
    };
    this.fieldSettingDialog = true;
  }

  saveFieldSetting(){    
    if(this.fieldSetting.value == 1){
      this.selectedField.mandatory = true;
      this.selectedField.readOnly = false;
    }
    if(this.fieldSetting.value == 2){
      this.selectedField.readOnly = true;
      this.selectedField.mandatory = false;
    }      
    if(this.fieldSetting.value == 100){
      this.selectedField.readOnly = false;
      this.selectedField.mandatory = false;
    }
    this.fieldSettingDialog = false;
    this.selectedField = "";
    this.fieldSetting = {
      value:100
    };
  }

  cancelFieldSetting(){
    this.fieldSettingDialog = false;
    this.fieldSetting = {
      value:100
    };
    this.selectedField = "";
  }

  openLayoutDialog(){
    this.saveLayoutDialog = true;
    if(this.layoutId != null)
      this.layoutName = this.mainLayout.layoutName;
  }

  onLayoutSave(){    
    if(this.layoutName != null){
      this.mainLayout.layoutName = this.layoutName; 
    this.mainLayout.object_id = this.objectId;
      if(this.layoutId != null){
        this.layoutService.updateLayout(this.layoutId, this.mainLayout).subscribe(
          data=>{     
            this.saveLayoutDialog = false;         
            this.location.back();
          }
        );
      }  
      else{        
        this.layoutService.insertLayout(this.mainLayout).subscribe(
          data=>{
            this.saveLayoutDialog = false;
            this.location.back();
          }
        );
      } 
    } 
  }

  cancelLayoutDialog(){
    this.saveLayoutDialog = false;
    this.layoutName = "";
  }

  showSectionDialog(section){
    this.sectionDialog = true;
    this.selectedSection = section;

    this.sectionData.sectionName = section.sectionName;
    this.sectionData.sectionColumns = section.columns.length;
    this.sectionData.sectionContent = section.sectionContent;
  }

  editSection(form){    
    if(this.sectionData.sectionName != undefined){
      this.selectedSection.sectionName = this.sectionData.sectionName;
      if(this.selectedSection.sectionContent != this.sectionData.sectionContent){
        this.selectedSection.columns.forEach(column => {
          column.columnData.forEach(data=>{
            this.firstBag.push(data);
          }) ;
          column.relatedItems.forEach(data=>{
            this.relatedItems.push(data);
          });
          
          column.columnData = [];
          column.relatedItems = [];
        });
      }
      this.selectedSection.sectionContent = this.sectionData.sectionContent;
      if(this.selectedSection.columns.length > this.sectionData.sectionColumns){
        let sectionDiff = this.selectedSection.columns.length - this.sectionData.sectionColumns;
        for(let i=1; i<=sectionDiff; i++){
          let data = this.selectedSection.columns.pop();
          data.columnData.forEach(item=>{
            this.selectedSection.columns[0].columnData.push(item);
          });
        }
      }
      else if(this.selectedSection.columns.length < this.sectionData.sectionColumns){
        let sectionDiff = this.sectionData.sectionColumns - this.selectedSection.columns.length;
        for(let i=1; i<=sectionDiff; i++){
          let column = new Columns;
          this.selectedSection.columns.push(column);
        }
      }
    }
    this.sectionDialog = false;
    
    form.reset();
    this.selectedSection = "";
  }

  styleMe(value, c, section?, column?){
    c.highlight = value;
    if(section!=undefined)
      this.section = section;
    if(column!=undefined)
      this.column = column;
  }

  sortColumnData($event, columnData, index){
  }

  handleItemDrop($event){
    let droppedData = $event.dragData;

    if(droppedData != undefined){
      if(droppedData.type == 'div'){
        this.sectionDialog = true;
      }
    }
  }

  createSection(layoutFor){
    switch(layoutFor){
      case "basic_search":
        this.saveSectionCustom("Basic Search", "fields", 1);
      break;
      case "advanced_search":
        this.saveSectionCustom("Advance Search", "fields", 3);
      break;
      case "search_result":
        this.saveSectionCustom("Search Result", "fields", 1);
      break;
    }
  }

  saveSectionCustom(sectionName, sectionContent, columns){
    let subSection = new SubSection;
    subSection.sectionName = sectionName;
    subSection.sectionContent = sectionContent;
    for(let i=1; i<=columns; i++){
      let column = new Columns;
      if(subSection.columns != undefined)
        subSection.columns.push(column);
      else
        subSection.columns = [column];
    }
    
    if(this.mainLayout.mainSection == undefined)
      this.mainLayout.mainSection = [subSection];
    else
      this.mainLayout.mainSection.push(subSection);
    
  }

  saveSection(form){
    if(this.sectionData.sectionName){
      let subSection = new SubSection;
      subSection.sectionName = this.sectionData.sectionName;
      subSection.sectionContent = this.sectionData.sectionContent;
      for(let i=1; i<=this.sectionData.sectionColumns; i++){
        let column = new Columns;
        if(subSection.columns != undefined)
          subSection.columns.push(column);
        else
          subSection.columns = [column];
      }
      
      if(this.mainLayout.mainSection == undefined)
        this.mainLayout.mainSection = [subSection];
      else
        this.mainLayout.mainSection.push(subSection);
    }
    this.sectionDialog = false;
    form.reset();
  }

  cancelSectionDialog(form){
    this.sectionDialog = false;
    console.log(form);
    form.reset();
    this.selectedSection = "";
  }

  goBack(){
    this.location.back();
  }
}

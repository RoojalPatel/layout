import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatelayoutComponent } from './createlayout.component';

describe('CreatelayoutComponent', () => {
  let component: CreatelayoutComponent;
  let fixture: ComponentFixture<CreatelayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatelayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatelayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

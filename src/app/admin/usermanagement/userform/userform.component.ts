import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { UsersService } from './../../services/users.service';
import { User } from './../../models/users.model';
import { Role } from '../../models/role.model';
import { RoleService } from '../../services/roles.service';
import {SelectItem} from 'primeng/api';
import { BusinessUnitService } from './../../services/businessunit.service';
import { BusinessUnit } from './../../models/businessunit.model';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})
export class UserformComponent implements OnInit {

  @Input() passedUser: User;

  public user:User = {
    userId: "",
    middleName: "",
    firstName: "",
    lastName: "",
    alias: "",
    userInfo: "",
    employeeId: "",
    phone: "",
    mobile: "",
    fax: "",
    email: "",
    password: "",
    buId: null,
    title: "",
    companyName: "",
    managerName: "",
    department: "",
    subDivision: "",
    building: "",
    desk: "",
    status: "",
    address1: "",
    address2: "",
    city: "",
    postalCode: "",
    state: "",
    country: ""
  }
  
  @ViewChild('userForm') userForm;

  buModel:SelectItem[] = [{label:'Select BU', value:null}];

  constructor(
    private userServ:UsersService,
    private buServ:BusinessUnitService
  ) { 
    this.buServ.getAllActiveBu().subscribe(
      (allBu:BusinessUnit[])=>{
        allBu.forEach(item=>{
          this.buModel.push({label:item.name, value:item.id});
        });
        
        if(this.passedUser != null)
          this.user = this.passedUser;
      }
    );
  }

  ngOnInit() {    
    this.userForm.reset();
  }

  onUserFormSubmit(userForm){
    this.clean(this.user);
    if(this.passedUser == null)
     this.userServ.createNewUser(this.user, userForm);
    else
      this.userServ.updateUser(this.user, this.user.id, userForm);
    
  }

  clean(obj) {
    for (var propName in obj) { 
      if (obj[propName] === null || obj[propName] === undefined || obj[propName]=="") {
        delete obj[propName];
      }
    }
  }

}

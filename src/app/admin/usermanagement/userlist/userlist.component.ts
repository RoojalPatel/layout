import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from './../../services/users.service';
import { User } from './../../models/users.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  @Input('used') used = 'internal';
  public cols = [
    { field: "userId", header: "User Id" },
    { field: "firstName", header: "First Name" },
    { field: "lastName", header: "Last Name" },
    { field: "email", header: "Email" },
    { field: "buName", header: "BU Name" }
  ];

  constructor(
    public userServ: UsersService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  goToCreateNew() {
    this.userServ.editDetails = false;
    this.userServ.showCreateNewUser = true;
    this.userServ.viewDetails = false;
    this.userServ.selectedUser = null;
  }

  getUserDetails(userId) {
    this.userServ.getUserById(userId).subscribe(
      (user: User) => {
        this.userServ.selectedUser = user;
        this.userServ.viewDetails = true;
        this.userServ.editDetails = false;
        this.userServ.showCreateNewUser = false;
        if (this.used != 'internal') {
          this.userServ.getAllUsers().subscribe(data => {
            this.userServ.users = data;
            this.router.navigateByUrl("admin/user-management");
          });
        }
      }
    );
  }
}

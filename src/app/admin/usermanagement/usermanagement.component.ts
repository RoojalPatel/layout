import { Component, OnInit, ViewChild } from '@angular/core';
import { Breadcrumbservice } from './../../services/utility/bread-crumb.service';
import { UsersService } from './../services/users.service';
import { User } from './../models/users.model';
import { FormGroup, FormControl } from '@angular/forms';
import { SearchCriteria } from './../models/searchcriteria.model';
import { MenuItem } from 'primeng/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { ConfirmationService } from 'primeng/api';
import { Observable } from 'rxjs/Observable';
import { GrantsService } from '../services/grants.service';
import { Grant } from '../models/grant.model';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.css']
})
export class UsermanagementComponent implements OnInit {

  public form:FormGroup;
  public userIdControl = new FormControl("");
  public firstNameControl = new FormControl("");
  public lastNameControl = new FormControl("");
  public userSearchLoading = false;
  public userGrants:Grant[];
  public allGrants:Observable<Grant[]>;
  public newUserGrant;

  public addNewMenuItems:MenuItem[];


  constructor(
    private breadCrumbServ:Breadcrumbservice,
    private userServ:UsersService,
    private messageServ:MessageService,
    private confServ: ConfirmationService,
    private grantServ:GrantsService
  ) { 
    this.form = new FormGroup({
      "userId":this.userIdControl,
      "firstName": this.firstNameControl,
      "lastName": this.lastNameControl
    })
  }

  ngOnInit() {
    this.breadCrumbServ.breadCrumbDataModel = [
      {label: "Admin"},
      {label: "User Management"}
    ];

    this.addNewMenuItems = [
      {label: 'New', icon: 'fa-plus'}
    ]
  }

  editBtnClicked(){
    this.userServ.editDetails = true;
    this.userServ.viewDetails = false;
  }

  deleteBtnClicked(){
    this.confServ.confirm({
      message:"Are you sure want to delete user : "+ this.userServ.selectedUser.userId,
      header: 'Confirmation',
      icon: 'fa fa-question-circle',
      reject: ()=>{
        this.messageServ.add({
          severity:"info",
          summary:"User Delete",
          detail:"User delete aborted !"
          });
      },
      accept: () =>{
        this.userServ.deleteUser(this.userServ.selectedUser.id).subscribe(data=>{
          this.messageServ.add({
            severity:"success",
            summary:"User Deleted",
            detail:"User details deleted successfully !"
            });
            this.userServ.selectedUser = null;
            this.userServ.viewDetails = false;
            this.userServ.editDetails = false;
            this.userServ.showCreateNewUser = false;
      
            this.userServ.getAllUsers().subscribe(
              (users:User[])=>{
                this.userServ.users = users;
              }
            );
        });
      }
    });
    
  }

  goToCreateNew(){
    this.userServ.editDetails = false;
    this.userServ.showCreateNewUser = true;
    this.userServ.viewDetails = false;
    this.userServ.selectedUser = null;
  }

  getUserDetails(userId){
    this.userServ.getUserById(userId).subscribe(
      (user:User)=>{
        this.userServ.selectedUser = user;
        this.userServ.viewDetails = true;
        this.userServ.editDetails = false;
        this.userServ.showCreateNewUser = false;
      }
    );
  }

  doUserSearch(){
    this.userSearchLoading = true;
    let searchCriteria = [];
    if(this.userIdControl.value){
      searchCriteria.push(new SearchCriteria("userId", "matching", this.userIdControl.value));
    }
    if(this.firstNameControl.value){
      searchCriteria.push(new SearchCriteria("firstName", "matching", this.firstNameControl.value));
    }
    if(this.lastNameControl.value){
      searchCriteria.push(new SearchCriteria("lastName", "matching", this.lastNameControl.value));
    }

    this.userServ.searchUsers({"searchCriteria":searchCriteria}).subscribe(
      (users:User[])=>{
        this.userServ.users = users;
        this.userSearchLoading = false;
      },
      error=>{
        this.userSearchLoading = false;
      }
    );
  }

  handleTabChange(e){
    if(e.index == 1){
        this.grantServ.getGrantByUser(this.userServ.selectedUser.id).subscribe(
          (grants:Grant[])=>{
            this.userGrants = grants;
          }
        );
        this.allGrants = this.grantServ.getAllGrants();
    }
  }

  addGrantToUser(){
    // this.grantServ.addGrantToUser(this.newUserGrant, this.selectedUser.id).subscribe(
    //   data=>{
    //     console.log(data);
    //   }
    // )
  }

}

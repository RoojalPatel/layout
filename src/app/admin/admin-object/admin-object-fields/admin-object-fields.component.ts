import { Component, OnInit } from '@angular/core';
import {Breadcrumbservice} from '../../../services/utility/bread-crumb.service';
import {AdminObjectUtilityService} from '../../services/utility/admin-object-utility.service';

@Component({
  selector: 'app-admin-object-fields',
  templateUrl: './admin-object-fields.component.html',
  styleUrls: ['./admin-object-fields.component.css']
})
export class AdminObjectFieldsComponent implements OnInit {

  selectedTabIndex = 0;

  public cols = [
    { field: 'name', header: 'Name' }
  ];

  constructor(public adminObjectUtilityService: AdminObjectUtilityService,
              private breadCrumb: Breadcrumbservice
  ) { }

  ngOnInit() {
  }

  selectObjectField(objectField) {
    this.adminObjectUtilityService.selectedField = objectField;
    this.breadCrumb.breadCrumbTreeDataModel.push({ label: objectField.name });
  }

  editObjectField(obj) {
    this.adminObjectUtilityService.showOnFormTab = 'Fields';
    this.adminObjectUtilityService.editMode = true;
    this.adminObjectUtilityService.selectedField = obj;
    this.adminObjectUtilityService.fieldTemplate.name = this.adminObjectUtilityService.selectedField.name;
    this.adminObjectUtilityService.fieldTemplate.description = this.adminObjectUtilityService.selectedField.description;
    this.adminObjectUtilityService.fieldTemplate.apiName = this.adminObjectUtilityService.selectedField.apiName;
    this.adminObjectUtilityService.fieldTemplate.help = this.adminObjectUtilityService.selectedField.help;
    this.adminObjectUtilityService.fieldTemplate.mandatory = this.adminObjectUtilityService.selectedField.mandatory;
    this.adminObjectUtilityService.fieldTemplate.secure = this.adminObjectUtilityService.selectedField.secure;
    this.adminObjectUtilityService.fieldTemplate.unique = this.adminObjectUtilityService.selectedField.unique;
    this.adminObjectUtilityService.fieldTemplate.defaultValue = this.adminObjectUtilityService.selectedField.defaultValue;
    this.adminObjectUtilityService.fieldTemplate.type = this.adminObjectUtilityService.selectedField.type;
    this.adminObjectUtilityService.fieldTemplate.length = this.adminObjectUtilityService.selectedField.length;
    this.adminObjectUtilityService.fieldTemplate.decimals = this.adminObjectUtilityService.selectedField.decimals;
    this.adminObjectUtilityService.fieldTemplate.autogen = this.adminObjectUtilityService.selectedField.autogen;
    this.adminObjectUtilityService.fieldTemplate.lov = this.adminObjectUtilityService.selectedField.lov;
    this.adminObjectUtilityService.fieldTemplate.formula = this.adminObjectUtilityService.selectedField.formula;
    this.adminObjectUtilityService.fieldTemplate.refObject = this.adminObjectUtilityService.selectedField.refObject;
    this.adminObjectUtilityService.fieldTemplate.basicSearch = this.adminObjectUtilityService.selectedField.basicSearch;
    this.adminObjectUtilityService.fieldTemplate.advancedSearch = this.adminObjectUtilityService.selectedField.advancedSearch;
    this.adminObjectUtilityService.fieldTemplate.searchType = this.adminObjectUtilityService.selectedField.searchType;
    this.adminObjectUtilityService.showFormTab = true;
  }

  deleteObjectField(id) {
    if (this.adminObjectUtilityService.selectedField) {
      if (this.adminObjectUtilityService.selectedField.id === id) {
        this.adminObjectUtilityService.selectedField = null;
      }
    }
    this.adminObjectUtilityService.deleteAdminObjectField(id);
  }

  handleChange(e) {
    this.selectedTabIndex = e.index;
  }

}

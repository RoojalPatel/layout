import { Component, OnInit } from '@angular/core';
import {AdminObjectUtilityService} from '../../services/utility/admin-object-utility.service';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-admin-object-form',
  templateUrl: './admin-object-form.component.html',
  styleUrls: ['./admin-object-form.component.css']
})
export class AdminObjectFormComponent implements OnInit {

  TYPES = ['Auto', 'Check Box', 'Currency', 'Date', 'Date Time', 'Email', 'Formula', 'Reference', 'Geo Location', 'Number', 'Big Number', 'String', 'Percent', 'Real', 'Big Real', 'Pick List', 'Multi Pick List'];

  FIELD_SEARCH_TYPE = ['Exact', 'Contains', 'Range'];

  constructor(public adminObjectUtilityService: AdminObjectUtilityService,
              public messageSerive: MessageService) { }

  ngOnInit() {
  }

  onChangeType(newValue) {
  }

  createObject() {
    if (this.adminObjectUtilityService.editMode) {
      this.adminObjectUtilityService.editAdminObject(Object.assign({}, this.adminObjectUtilityService.selectedObject, {
        objectName: this.adminObjectUtilityService.objectTemplate.objectName,
        description: this.adminObjectUtilityService.objectTemplate.description,
        apiName: this.adminObjectUtilityService.objectTemplate.apiName,
        helpText: this.adminObjectUtilityService.objectTemplate.helpText,
        updateDate: new Date(),
        updatedBy: 'Admin'
      }));
    } else {
      this.adminObjectUtilityService.addAdminObject(Object.assign({}, this.adminObjectUtilityService.objectTemplate, {
        createDate: new Date(),
        createdBy: 'Admin'
      }));
    }
  }

  createField() {
    if (this.adminObjectUtilityService.editMode) {
      this.adminObjectUtilityService.editAdminObjectField(Object.assign({}, this.adminObjectUtilityService.selectedField, {
        name: this.adminObjectUtilityService.fieldTemplate.name,
        description: this.adminObjectUtilityService.fieldTemplate.description,
        apiName: this.adminObjectUtilityService.fieldTemplate.apiName,
        help: this.adminObjectUtilityService.fieldTemplate.help,
        mandatory: this.adminObjectUtilityService.fieldTemplate.mandatory,
        secure: this.adminObjectUtilityService.fieldTemplate.secure,
        unique: this.adminObjectUtilityService.fieldTemplate.unique,
        defaultValue: this.adminObjectUtilityService.fieldTemplate.defaultValue,
        type: this.adminObjectUtilityService.fieldTemplate.type,
        length: this.adminObjectUtilityService.fieldTemplate.length,
        decimals: this.adminObjectUtilityService.fieldTemplate.decimals,
        autogen: this.adminObjectUtilityService.fieldTemplate.autogen,
        sequenceName: this.adminObjectUtilityService.fieldTemplate.sequenceName,
        format: this.adminObjectUtilityService.fieldTemplate.format,
        lov: this.adminObjectUtilityService.fieldTemplate.lov,
        formula: this.adminObjectUtilityService.fieldTemplate.formula,
        refObject: this.adminObjectUtilityService.fieldTemplate.refObject,
        basicSearch: this.adminObjectUtilityService.fieldTemplate.basicSearch,
        advancedSearch: this.adminObjectUtilityService.fieldTemplate.advancedSearch,
        searchType: this.adminObjectUtilityService.fieldTemplate.searchType
      }));
    } else {
      this.adminObjectUtilityService.addAdminObjectField(Object.assign({}, this.adminObjectUtilityService.fieldTemplate, {
        objectId: this.adminObjectUtilityService.selectedObject.id,
        createDate: new Date(),
        createdBy: 'Admin'
      }));
    }
  }

}

import { Component, OnInit } from '@angular/core';
import {AdminObjectUtilityService} from '../../services/utility/admin-object-utility.service';
import {Breadcrumbservice} from '../../../services/utility/bread-crumb.service';

@Component({
  selector: 'app-admin-object-details',
  templateUrl: './admin-object-details.component.html',
  styleUrls: ['./admin-object-details.component.css']
})
export class AdminObjectDetailsComponent implements OnInit {

  constructor(public adminObjectUtilityService: AdminObjectUtilityService,
              private breadCrumb: Breadcrumbservice
  ) { }

  ngOnInit() {
  }

}

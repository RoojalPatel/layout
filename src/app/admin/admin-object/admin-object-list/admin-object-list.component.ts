import { Component, OnInit } from '@angular/core';
import {AdminObjectsService} from '../../services/admin-object.service';
import {AdminObjectUtilityService} from '../../services/utility/admin-object-utility.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {Breadcrumbservice} from '../../../services/utility/bread-crumb.service';
import { ObjectsUtilityService } from '../../services/utility/objects-utility.service';

@Component({
  selector: 'app-admin-object-list',
  templateUrl: './admin-object-list.component.html',
  styleUrls: ['./admin-object-list.component.css']
})
export class AdminObjectListComponent implements OnInit {

  isStandard = true;
  showSidebar = false;
  public cols = [
    { field: 'name', header: 'Name' }
  ];

  constructor(
    public adminObjectsService: AdminObjectsService,
    public adminObjectsUtilityService: AdminObjectUtilityService,
    public messageService: MessageService,
    private breadCrumb: Breadcrumbservice,

    private objectUtService: ObjectsUtilityService
  ) { }

  ngOnInit() {
  }

  selectObject(object) {
    this.adminObjectsUtilityService.selectedObject = object;
    this.breadCrumb.breadCrumbTreeDataModel = [{ label: object.objectName }];
    this.adminObjectsUtilityService.getAllFieldsOfObject(this.adminObjectsUtilityService.selectedObject.id);
    this.adminObjectsUtilityService.showDetails = true;

    this.objectUtService.selectedObject = object;
  }

  onAddObject() {
    this.adminObjectsUtilityService.editMode = false;
    this.adminObjectsUtilityService.resetObjectTemplate();
    this.adminObjectsUtilityService.showOnFormTab = 'Objects';
    this.adminObjectsUtilityService.editMode = false;
    this.adminObjectsUtilityService.showFormTab = true;
  }

  deleteObject(id) {
    if (this.adminObjectsUtilityService.selectedObject) {
      if (this.adminObjectsUtilityService.selectedObject.id === id) {
        this.adminObjectsUtilityService.showDetails = false;
        this.adminObjectsUtilityService.selectedObject = null;
      }
    }
    this.adminObjectsUtilityService.deleteAdminObject(id);
  }

}

import { Injectable } from "@angular/core";
import { User } from "../models/users.model";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MessageService } from 'primeng/components/common/messageservice';

@Injectable({
  providedIn:"root"
})
export class UsersService{

  public users:User[];
  public userSearchLoading = false;
  public selectedUser:User;
  public viewDetails = false;
  public editDetails = false;
  public showCreateNewUser = false;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(
    private http:HttpClient,
    private messageServ:MessageService
  ){
    this.userSearchLoading = true;
    this.getAllUsers().subscribe(
      users=>{
        this.userSearchLoading = false;
        this.users = users;
      },
      error=>{
        this.userSearchLoading = false;
      }
    );
  }

  getAllUsers():Observable<any>{
    return this.http.get<User[]>("/api/users");
  }

  getUserById(id):Observable<any>{
    return this.http.get<User>("/api/users/"+id);
  }

  searchUsers(searchCriteria):Observable<any>{
    return this.http.post("/api/users/search", JSON.stringify(searchCriteria), this.httpOptions);
  }

  createNewUser(newUser:User, uForm){
    this.http.post("/api/users", JSON.stringify(newUser), this.httpOptions)
    .subscribe(data=>{
      this.messageServ.add({
      severity:"success",
      summary:"User Created",
      detail:"User Creation Successful !"
      });
      uForm.reset();

      this.getAllUsers().subscribe(
        (users:User[])=>{
          this.users = users;
        }
      );
    });
  }

  updateUser(updatedUser:User, userId, uForm){
    this.http.put("/api/users/"+userId, JSON.stringify(updatedUser), this.httpOptions)
    .subscribe(data=>{
      this.messageServ.add({
      severity:"success",
      summary:"User Updated",
      detail:"User details updated successfully !"
      });
      uForm.reset();

      this.getAllUsers().subscribe(
        (users:User[])=>{
          this.users = users;
        }
      );
    });
  }
  
  deleteUser(userId):Observable<any>{
    return this.http.delete("/api/users/"+userId);
  }
}
import { MessageService } from 'primeng/components/common/messageservice';
import { RoleService } from './../roles.service';
import { Role } from './../../models/role.model';
import { Injectable } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { TreeNodeService } from './tree-node.service';
import { ObjectsService } from './../objects.service';
import * as _ from 'lodash';
import { combineLatest } from 'rxjs';
import { ObjectName } from '../../models/objectname.model';
import { Grant } from '../../models/grant.model';
import { ObjectsUtilityService } from './objects-utility.service';
import { Breadcrumbservice } from '../../../services/utility/bread-crumb.service';

@Injectable({
  providedIn: 'root'
}) 
export class RoleutilityService {
  public treeTableLoading = false;
  public roleDataLoading = false;
  public allRoles: Role[];
  public allGrants: Grant[];
  public showDetails = false;
  public showCreateNew = false;
  public rootRole: Role;
  public selectedRole: Role;
  public rootNodes: TreeNode[];

  constructor(
    public roleServ: RoleService,
    public msgServ: MessageService,
    public treeNodeServ: TreeNodeService,
    public objectService: ObjectsService,
    public objectUtService: ObjectsUtilityService,
    private breadCrumb: Breadcrumbservice
  ) { 
    this.getAll();
  }

  deepcopy<T>(o: T): T {
    return JSON.parse(JSON.stringify(o));
  }

  selectEmptyRole() {
    this.selectedRole = {
      name: "",
      description: "",
      createDate: new Date().toDateString(),
      parentId: null,
      root: false,
      active: false
    };
  }

  getAll() {
    this.roleServ.getAllRoles().subscribe(roles => {
      // this.allRoles =  _.filter(roles, function(item){
      //   return item.parentId === null;
      // });
      this.allRoles = roles;
    });
  }

  getImmediateChildren(id: number): void {
    this.treeTableLoading = true;
    this.roleServ.getChildren(id).subscribe(data => {
      // this.immediateChildren = data;
      this.rootNodes = this.treeNodeServ.prepareTreeNode(data);
      this.rootNodes = [...this.rootNodes];
      this.treeTableLoading = false;
    });
  }
  
  getChildNodes(node: TreeNode): void {
    this.treeTableLoading = true;
    node.children = [];
    this.roleServ.getChildren(node.data.id).subscribe(data => {
      node.children = this.treeNodeServ.prepareTreeNode(data);
      this.rootNodes = [...this.rootNodes];
      this.treeTableLoading = false;
    });
  }

  addChildren(roleId: number, parentId: number){
    this.roleServ.addChildren(roleId, parentId).subscribe(res =>{}, err =>{console.log(err);});
  }

  resetRoleView(){
    this.selectedRole = this.rootRole;

    this.getImmediateChildren(this.selectedRole.id);
    this.fetchRoleData();

    this.breadCrumb.breadCrumbTreeDataModel = [
      // { label: this.selectedRole.name }
      this.breadCrumb.generateBreadcrumbItem(this.selectedRole)
    ];
  }

  fetchRoleData(){
    let role = this.selectedRole;
    this.getObjects(role.id);

    let allGrant = this.roleServ.getAllGrants(role.id);
    let assignGrant = this.roleServ.getAssignedGrants(role.id);

    const self = this;
    combineLatest(allGrant, assignGrant).subscribe(res => {
      let allGrantRes = res[0];
      let assignGrantRes = res[1];

      this.allGrants =allGrantRes;
      this.selectedRole.grants =assignGrantRes;
      self.roleDataLoading = false;
    // role.unassigngrants = self.getUnassignedGrants(allGrantRes);
    });
  }
  getObjects(roleId: number) {
    this.roleServ.getRoleObjects(roleId).subscribe(roleobjects => {
      this.selectedRole.roleobjects = roleobjects;
      
      this.objectUtService.selectedObject = roleobjects[0].object;
      if(this.objectUtService.selectedObject){
        this.objectUtService.getFGrants(roleId, this.objectUtService.selectedObject.id);
        this.objectUtService.getLGrants(roleId, this.objectUtService.selectedObject.id);
      }
    });
  }

  updateObjectAccessLevel(roleObjectId:number, leveltext:string, level:number){
    
    let payload = "{\""+leveltext+"\":"+level+"}";
    this.roleServ.updateObjectAccessLevel(roleObjectId, payload).subscribe(
      res => {  
        this.roleDataLoading =false;
      },
      error => { console.log(error);}
    );
  }

  getAllGrants(roleId: number) {
    this.roleServ.getAllGrants(roleId).subscribe(grants => {
      this.allGrants =grants;
    });
  }

  getAssignedGrants(roleId: number) {
    this.roleServ.getAssignedGrants(roleId).subscribe(grants => {
      this.selectedRole.grants =grants;
    });
  }

  // getUnassignedGrants(allGrants: Grant[]): Grant[]{
  //   let unassignedGrants=[];
  //   _.forEach(allGrants, function(grant){
  //     if(grant.roleId==undefined || grant.roleId==null){
  //       unassignedGrants.push(grant);
  //     }
  //   });

  //   return unassignedGrants;
  // }

  // enableRole(roleId) {
  //   this.roleServ.enableRole(roleId).subscribe(data => {
  //     this.msgServ.add({
  //       severity: "success",
  //       summary: "Role",
  //       detail: "Role Activated !"
  //     });
  //     this.getAll();
  //   });
  // }
  

  // disableRole(roleId) {
  //   this.roleServ.disableRole(roleId).subscribe(data => {
  //     this.msgServ.add({
  //       severity: "success",
  //       summary: "Role",
  //       detail: "Role Deactivated !"
  //     });
  //     this.getAll();
  //   });
  // }

  enableRoleForobject(roleID, objectID, buID) {
    // this.roleServ.enableRole(roleID).subscribe(data => {
    //   this.msgServ.add({
    //     severity: "success",
    //     summary: "Role",
    //     detail: "Role Activated !"
    //   });
      // this.getAllList();
    // });
  }

  disableRoleForobject(roleID, objectID, buID) {
    // this.roleServ.disableRole(roleID).subscribe(data => {
    //   this.msgServ.add({
    //     severity: "success",
    //     summary: "Role",
    //     detail: "Role Deactivated !"
    //   });
      // this.getAllList();
    // });
  }
}

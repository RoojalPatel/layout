import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenericutilService {

  constructor() { }

  public replaceFieldName(statement: string, fieldName: string, cursorPosition: number) {
    let selectionStart = cursorPosition,
        leftWordBoundary = selectionStart,
        rightwordBoundary = selectionStart,
        command = statement,
        leftPattern = /[^\(,]/,
        rightPattern = /[^\),]/;
  
      if (command.charAt(leftWordBoundary) == "" || command.charAt(leftWordBoundary)== ",") 
      {
        leftWordBoundary--;
      }
      
      while (command.charAt(leftWordBoundary).match(leftPattern)) {
        leftWordBoundary--;
      }
      let functionEnd =  command.indexOf(")", leftWordBoundary+1);
  
      //If "(" OR ")" is on same index that means cursor position is out of argument
      if(leftWordBoundary === -1 || selectionStart === leftWordBoundary || functionEnd<selectionStart){
        return statement;
      }
  
      while (command.charAt(rightwordBoundary).match(rightPattern)) {
        rightwordBoundary++;
      }
      
      return command.substring(0, leftWordBoundary+1) + fieldName + command.substring(rightwordBoundary);
  }
}

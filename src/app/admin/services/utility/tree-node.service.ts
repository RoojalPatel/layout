import { TreeNode } from 'primeng/api';
import { BusinessUnitService } from './../businessunit.service';
import { BusinessUnit } from './../../models/businessunit.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn:"root"
})
export class TreeNodeService{

  prepareTreeNode(data){
    let treeNode:TreeNode[] = [];
    data.forEach(element => {
      treeNode.push({
        data:element,
        leaf:false,
        children:[]
      });      
    });
    return treeNode;
  }
}
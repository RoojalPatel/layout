import { RecordType } from './../../models/recordtype.model';
import { Layout } from './../../models/layout.model';
import { Field } from './../../models/field.model';
import { ObjectName } from './../../models/objectname.model';
import { Injectable } from '@angular/core';
import { ObjectsService } from './../objects.service';
import { RoleService } from './../roles.service';
import { combineLatest } from 'rxjs';
import * as _ from 'lodash';
import { RoleObject } from '../../models/roleObject.model';
import { ValidationRule } from '../../models/validation-rule.model';
import { BusinessRule } from '../../models/business-rule.model';
import { Function } from '../../models/function.model';

@Injectable({
  providedIn: 'root'
})
export class ObjectsUtilityService {
  public roleObjectDataLoading = false;
  public vRuleEditMode = false;
  public bRuleEditMode = false;

  public allFunctions: Function[];
  public selectedObject: ObjectName;
  public selectedRecordType: RecordType;
  public selectedVRule: ValidationRule;
  public selectedBRule: BusinessRule;

  public objectColorCodeClass = [];

  constructor(
    public objectService: ObjectsService,
    public roleServ: RoleService
  ) { 
    this.objectColorCodeClass[0]={ class: 'text-danger fa fa-circle-o', name: 'None' };
    this.objectColorCodeClass[2]={ class: 'text-warning fa fa-circle-o', name: 'User' };
    this.objectColorCodeClass[4]={ class: 'text-success fa fa-circle-o', name: 'Business Unit' };
    this.objectColorCodeClass[8]={ class: 'text-success fa fa-circle', name: 'Business Unit Hierarchy' };
    this.objectColorCodeClass[16]={ class: 'text-warning fa fa-circle', name: 'Corporate' };
  }

  deepcopy<T>(o: T): T {
    return JSON.parse(JSON.stringify(o));
  }

  getFields(objectId: number) {
    this.objectService.getFields(objectId).subscribe(fields => {
      this.selectedObject.fields = fields;
    });
  }

  getFGrants(roleId: number, objectId: number) {
    this.objectService.getFGrants(roleId, objectId).subscribe(res => {
      this.selectedObject.fgrants = res;
    });
  }

  getLGrants(roleId: number, objectId: number) {
    this.objectService.getLGrants(roleId, objectId).subscribe(lgrants => {
      this.selectedObject.lgrants = lgrants;
    });
  }

  enableFGrant(fgrantId: number) {
    this.objectService.enableFGrant(fgrantId).subscribe(field => {
      this.roleObjectDataLoading =false;
    });;
  }

  disableFGrant(fgrantId: number) {
    this.objectService.disableFGrant(fgrantId).subscribe(field => {
      this.roleObjectDataLoading =false;
    });
  }

  enableLGrant(lgrantId: number) {
    this.objectService.enableLGrant(lgrantId).subscribe(layout => {
      this.roleObjectDataLoading =false;
    });
  }

  disableLGrant(lgrantId: number) {
    this.objectService.disableLGrant(lgrantId).subscribe(layout => {
      this.roleObjectDataLoading =false;
    });
  }

  /****** Object Modle ********/
  getRecordTypes(objectId: number){
    this.objectService.getRecordTypes(objectId).subscribe(
      recordtypes => {
        this.selectedObject.recordtypes = recordtypes;

        this.selectedRecordType = recordtypes[0];
        if(this.selectedRecordType){
          this.getLayouts(this.selectedRecordType.id);
        }
        this.roleObjectDataLoading = false;
      }
    );
  }

  getLayouts(recordtypeId: number){
    this.objectService.getLayouts(recordtypeId).subscribe(
      layouts => {
        this.selectedRecordType.layouts = layouts;

        this.roleObjectDataLoading = false;
      }
    );
  }

  getRoles(objectId: number){
    this.objectService.getRoles(objectId).subscribe(
      roleObjects => {
        this.selectedObject.roleObjects = roleObjects;
        this.roleObjectDataLoading = false;
      }
    );
  }

  getVRules(objectId: number){
    this.objectService.getVRules(objectId).subscribe(
      vrules => {
        this.selectedObject.vrules = vrules;
        this.roleObjectDataLoading = false;
      }
    );
  }

  deleteVRule(vruleId: number){
    this.objectService.getVRules(vruleId).subscribe(
      vrules => {
        _.remove(this.selectedObject.vrules, function(vrule){
            return vrule.id == vruleId;
        });
        
        this.roleObjectDataLoading = false;
      }
    );
  }

  selectEmptyVRule() {
    this.selectedVRule = {
      name: "",
      statement:"",
      createDate: new Date().toDateString(),
      createdBy: 1,
      active:true
    };
  }

  selectEmptyBRule() {
    this.selectedBRule = {
      name: "",
      action:null,
      insert:false,
      update:false,
      delete:false,
      createDate: new Date().toDateString(),
      createdBy: 1,
      active:true
    };
  }

  getBRules(objectId: number){
    this.objectService.getBRules(objectId).subscribe(
      brules => {
        this.selectedObject.brules = brules;
        this.roleObjectDataLoading = false;
      }
    );
  }

  deleteBRule(bruleId: number){
    this.objectService.getBRules(bruleId).subscribe(
      brules => {
        _.remove(this.selectedObject.brules, function(brule){
            return brule.id == bruleId;
        });
        
        this.roleObjectDataLoading = false;
      }
    );
  }
}

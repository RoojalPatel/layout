import { MessageService } from 'primeng/components/common/messageservice';
import { Subject } from 'rxjs';
import { TreeNode } from 'primeng/api';
import { TreeNodeService } from './tree-node.service';
import { BusinessUnitService } from './../businessunit.service';
import { BusinessUnit } from './../../models/businessunit.model';
import { Injectable } from '@angular/core';
import { Role } from '../../models/role.model';
import { RoleService } from '../roles.service';
import { RoleutilityService } from './roleutility.service';
import { ObjectName } from '../../models/objectname.model';

@Injectable({
  providedIn: "root"
})
export class BuUtilityService {

  public allBusinessUnits: BusinessUnit[];
  public allActiveBu: BusinessUnit[];
  public selectedBu: BusinessUnit;
  public showDetails: boolean = false;
  public showCreateNew: boolean = false;
  public immediateChildren: BusinessUnit[];
  public rootNodes: TreeNode[];
  public treeTableLoading = false;
  public roleDataLoading = false;
  
  // BN
  public selectedObject: ObjectName;
  public assignedRoles: Role[];
  public allRoles: Role[];
  public objectNames: ObjectName[];

  public buTemplate = {
    name: "",
    description: "",
    parentId: "",
    active: true
  };

  constructor(
    public buServ: BusinessUnitService,
    public treeNodeServ: TreeNodeService,
    public msgServ: MessageService,
    public roleServ: RoleService,
    public roleUtiServ: RoleutilityService
  ) {
    this.getAllList();
    this.getAllActiveBu();

  }

  resetBuTemplate() {
    this.buTemplate = {
      name: "",
      description: "",
      parentId: "",
      active: true
    };
  }

  enableBu(buId) {
    this.buServ.enableBu(buId).subscribe(data => {
      this.msgServ.add({
        severity: "success",
        summary: "Business Unit",
        detail: "Business Unit Activated !"
      });
      this.getAllList();
    });
  }

  disableBu(buId) {
    this.buServ.disableBu(buId).subscribe(data => {
      this.msgServ.add({
        severity: "success",
        summary: "Business Unit",
        detail: "Business Unit Deactivated !"
      });
      this.getAllList();
    });
  }

  deleteBuById(buId) {
    this.buServ.deleteBu(buId).subscribe(data => {
      this.msgServ.add({
        severity: "success",
        summary: "Business Unit",
        detail: "Business Unit Deleted !"
      });
      this.getAllList();
      if (this.selectedBu.id == buId) {
        this.selectedBu = null;
        this.showDetails = false;
      }
    });
  }

  getAllList(): void {
    this.buServ.getAllBusinessUnits().subscribe(
      buUnits => {
        this.allBusinessUnits = buUnits;
      }
    );
  }

  getAllActiveBu() {
    this.buServ.getAllActiveBu().subscribe(
      buUnits => {
        this.allActiveBu = buUnits;
      }
    );
  }

  getAllActiveAsOptions() {
    let buOptions = [];
    this.allActiveBu.forEach(element => {
      buOptions.push({
        label: element.name,
        value: element.id
      });
    });
    return buOptions;
  }

  getImmediateChildren(id: number): void {
    this.treeTableLoading = true;
    this.buServ.getChildren(id).subscribe(data => {
      this.immediateChildren = data;
      this.rootNodes = this.treeNodeServ.prepareTreeNode(data);
      this.rootNodes = [...this.rootNodes];
      this.treeTableLoading = false;
    });
  }

  getChildNodes(node: TreeNode): void {
    this.treeTableLoading = true;
    node.children = [];
    this.buServ.getChildren(node.data.id).subscribe(data => {
      node.children = this.treeNodeServ.prepareTreeNode(data);
      this.rootNodes = [...this.rootNodes];
      this.treeTableLoading = false;
    });
  }

  // BN
  getAllRoles(id: number): void {
    this.roleServ.getAllRoles().subscribe(roles => {
      this.allRoles = roles;
      // this.getAssignedRoles(id);
    });
  }
  getAssignedRoles(id: number): void {
    this.roleDataLoading = true;
    this.assignedRoles = [];
    this.allRoles = [];

    this.buServ.getAllAssignedRoles(id).subscribe(data => {
      this.assignedRoles = data;
      const assignedIds = data.map(x => x.roleId);
      this.roleServ.getAllRoles().subscribe(roles => {
        this.allRoles = roles.filter(x => !assignedIds.includes(x.id));
        this.roleDataLoading = false;
      });
    });
  }
  getNetObjects(id: number): void {
    // this.buServ.getAllNetObjects(id).subscribe(data => {
    //   this.allRoles = data;
    // });

    //TODO: will remove once all service work properly
    this.objectNames = [];
    // for(let i=1;i<=12;i++){
    //   this.objectNames.push({
    //     'id': i, 
    //     'name': 'Object Name '+i,
    //     'create':'none',
    //     'read':'user',
    //     'update':'bu',
    //     'delete':'buhierarchy',
    //     'fields':null,
    //     'recordtypes':null
    //   });
    // }
  }

  getNetRoles(id: number): void {
    this.roleDataLoading = true;
    
    this.buServ.getAllNetRoles(id).subscribe(data => {
      this.allRoles = data;
      this.roleDataLoading = false;
    });
  }

  getNetGrants(id: number): void {
    this.roleDataLoading = true;
    this.buServ.getAllNetGrants(id).subscribe(data => {
      this.allRoles = data;

      this.roleDataLoading = false;
    });
  }
  getNetOGrants(id: number): void {
    this.roleDataLoading = true;

    this.buServ.getAllNetOGrants(id).subscribe(data => {
      this.allRoles = data;
      this.roleDataLoading = false;
    });
  }
  getNetFGrants(id: number): void {
    this.roleDataLoading = true;

    this.buServ.getAllNetFgrants(id).subscribe(data => {
      this.allRoles = data;

      this.roleDataLoading = false;
    });
  }

  getNetLGrants(id: number): void {
    this.roleDataLoading = true;

    this.buServ.getAllNetLGrants(id).subscribe(data => {
      this.allRoles = data;

      this.roleDataLoading = false;
    });
  }
}

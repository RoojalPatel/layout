import { MessageService } from 'primeng/components/common/messageservice';
import { Subject } from 'rxjs';
import { Objects } from '../../models/admin-object.model';
import { Fields } from '../../models/admin-object.model';
import { Injectable } from '@angular/core';

import { AdminObjectsService } from '../admin-object.service';
import { MockAdminObjectsService } from '../../mockAPIs/adminObjectMockApi'; // Mock service until server APIs are live

@Injectable({
  providedIn: 'root'
})

export class AdminObjectUtilityService {

  public editMode = false;
  public allObjects: Objects[];
  public selectedObject: Objects;

  public TypeConstants = {
    AUTO: 'Auto',
    CHECH_BOX: 'Check Box',
    CURRENCY: 'Currency',
    DATE: 'Date',
    DATE_TIME: 'Date Time',
    EMAIL: 'Email',
    FORMULA: 'Formula',
    REFERENCE: 'Reference',
    GEO_LOCATION: 'Geo Location',
    NUMBER: 'Number',
    BIG_NUMBER: 'Big Number',
    STRING: 'String',
    PERCENT: 'Percent',
    REAL: 'Real',
    BIG_REAL: 'Big Real',
    PICK_LIST: 'Pick List',
    MULTI_PICK_LIST: 'Multi Pick List'
  };

  public allFields: Fields[];
  public selectedField: Fields;

  public showFormTab = false;
  public showOnFormTab = 'Objects'; // Change to any string for showing content on modal
  public showDetails = false;

  public objectTemplate = {
    objectName: '',
    description: '',
    apiName: '',
    helpText: '',
    objectCode: '',
    isStandard: true
  };

  public fieldTemplate = {
    name: '',
    description: '',
    apiName: '',
    help: '',
    mandatory: false,
    secure: false,
    unique: false,
    defaultValue: '',
    type: undefined,
    length: undefined,
    decimals: undefined,
    autogen: false,
    sequenceName: null,
    format: null,
    lov: '',
    formula: '',
    refObject: undefined,
    basicSearch: false,
    advancedSearch: false,
    searchType: '',
    custom: false
  };

  constructor(
    private messageService: MessageService,
    private adminObjectsService: AdminObjectsService,
    // private adminObjectsService: MockAdminObjectsService
  ) {
    this.getAllObjects();
  }

  resetObjectTemplate() {
    this.objectTemplate = {
      objectName: '',
      description: '',
      apiName: '',
      helpText: '',
      objectCode: '',
      isStandard: true
    };
  }

  resetFieldTemplate() {
    this.fieldTemplate = {
      name: '',
      description: '',
      apiName: '',
      help: '',
      mandatory: false,
      secure: false,
      unique: false,
      defaultValue: '',
      type: undefined,
      length: undefined,
      decimals: undefined,
      autogen: false,
      sequenceName: null,
      format: null,
      lov: '',
      formula: '',
      refObject: undefined,
      basicSearch: false,
      advancedSearch: false,
      searchType: '',
      custom: false
    };
  }

  getAllObjects() {
    this.adminObjectsService.getAllAdminObjects().subscribe(data => {
      // console.log(data);
      this.allObjects = data;
    }, err => {
      console.log(err);
    });
  }

  addAdminObject(obj) {
    this.adminObjectsService.addAdminObject(obj).subscribe(data => {
      this.showFormTab = false;
      this.resetObjectTemplate();
      this.messageService.add({
        severity: 'success',
        summary: 'Admin Object',
        detail: 'Admin Object added successfully'
      });
      this.getAllObjects();
    }, err => {
      console.log(err);
    });
  }

  editAdminObject(obj) {
    this.adminObjectsService.editAdminObject(obj.id, obj).subscribe(data => {
      this.showFormTab = false;
      this.messageService.add({
        severity: 'success',
        summary: 'Admin Object',
        detail: 'Admin Object edited successfully'
      });
      this.resetObjectTemplate();
      this.getAllObjects();
    }, err => {
      console.log(err);
    });
  }

  deleteAdminObject(id) {
    this.adminObjectsService.deleteAdminObject(id).subscribe(data => {
      this.messageService.add({
        severity: 'success',
        summary: 'Admin Object',
        detail: 'Admin Object deleted successfully'
      });
      this.getAllObjects();
    }, err => {
      console.log(err);
    });
  }

  getAllFieldsOfObject(id) {
    this.adminObjectsService.getAllAdminObjectsFields(id).subscribe(data => {
      // console.log(data);
      this.allFields = data;
    }, err => {
      console.log(err);
    });
  }

  addAdminObjectField(obj) {
    this.adminObjectsService.addAdminObjectField(obj).subscribe(data => {
      this.showFormTab = false;
      this.getAllFieldsOfObject(this.selectedObject.id);
      this.resetObjectTemplate();
      this.messageService.add({
        severity: 'success',
        summary: 'Admin Object',
        detail: 'Admin Object Field added successfully'
      });
    }, err => {
      console.log(err);
    });
  }

  editAdminObjectField(obj) {
    this.adminObjectsService.editAdminObjectField(obj.id, Object.assign({}, obj, {objectId: this.selectedObject.id})).subscribe(data => {
      this.showFormTab = false;
      this.getAllFieldsOfObject(this.selectedObject.id);
      this.resetFieldTemplate();
      this.messageService.add({
        severity: 'success',
        summary: 'Admin Object',
        detail: 'Admin Object Field edited successfully'
      });
    }, err => {
      console.log(err);
    });
  }

  deleteAdminObjectField(id) {
    this.adminObjectsService.deleteAdminObjectField(id).subscribe(data => {
      console.log(data);
      this.getAllFieldsOfObject(this.selectedObject.id);
      this.messageService.add({
        severity: 'success',
        summary: 'Admin Object',
        detail: 'Admin Object Field deleted successfully'
      });
    }, err => {
      console.log(err);
    });
  }
}

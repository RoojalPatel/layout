import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Objects, Fields } from '../models/admin-object.model';


@Injectable({
  providedIn: 'root'
})

export class AdminObjectsService {

  private url = 'http://localhost:3000';
  // private url = '/api';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) { }

  getAllAdminObjects(): Observable<Objects[]> {
    return this.http.get<Objects[]>(this.url+'/objects');
  }

  addAdminObject(payload){
    return this.http.post(this.url + '/objects', payload, this.httpOptions);
  }

  editAdminObject(objId:number, payload){
    return this.http.put(this.url + '/objects/' + objId, payload, this.httpOptions);
  }

  deleteAdminObject(objId: number){
    return this.http.delete(this.url + '/objects/' + objId, this.httpOptions);
  }

  getAllAdminObjectsFields(objId: number): Observable<Fields[]> {
    return this.http.get<any>(this.url+'/fields?objectId=' + objId, this.httpOptions);
  }

  addAdminObjectField(payload){
    return this.http.post(this.url + '/fields', payload, this.httpOptions);
  }

  editAdminObjectField(objId:number, payload){
    return this.http.put(this.url + '/fields/' + objId, payload, this.httpOptions);
  }

  deleteAdminObjectField(objId: number){
    return this.http.delete(this.url + '/fields/' + objId, this.httpOptions);
  }
}

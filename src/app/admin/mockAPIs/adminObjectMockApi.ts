import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Fields, Objects} from '../models/admin-object.model';


@Injectable({
  providedIn: 'root'
})

export class MockAdminObjectsService {

  // Define Delay of network
  public delay = 500;

  // Initialize some data
  allFields = [
    {
      id: '1678903888666444289010',
      name: 'TestField1',
      description: 'this is test field one',
      apiName: 'testFieldAPI',
      helpText: 'none text',
      isMandatory: false,
      secure: true,
      isUnique: true,
      defaultValue: 'TYHU',
      type: 'STANDARD',
      length: 23,
      decimals: 76,
      isAutogen: true,
      lov: 'njioka',
      formula: 'HUGUGU+NDJNDV',
      refObject: '1678903456254376289010',
      isBasicSearch: true,
      isAdvancedSearch: false,
      searchType: 'Child',
      isCustom: true
    },
    {
      id: '1865342387653445566778',
      name: 'TestField2',
      description: 'this is test2 fields',
      apiName: 'apitest2',
      helpText: 'GUHUHUH',
      isMandatory: true,
      secure: false,
      isUnique: true,
      defaultValue: 'HIJIJIfd',
      type: 'CUSTOM',
      length: 23,
      decimals: 76,
      isAutogen: true,
      lov: 'GG',
      formula: 'bvdjsvbsdv',
      refObject: '1678903456254376289010',
      isBasicSearch: true,
      isAdvancedSearch: false,
      searchType: 'Child',
      isCustom: false
    },
    {
      id: '1678903888669988776655',
      name: 'TestField2',
      description: 'this is test2 fields',
      apiName: 'apitest2',
      helpText: 'GUHUHUH',
      isMandatory: true,
      secure: false,
      isUnique: true,
      defaultValue: 'HIJIJIfd',
      type: 'STANDARD',
      length: 23,
      decimals: 76,
      isAutogen: true,
      lov: 'GG',
      formula: 'bvdjsvbsdv',
      refObject: '18943200298354189523',
      isBasicSearch: true,
      isAdvancedSearch: false,
      searchType: 'Child',
      isCustom: false
    }
  ];
  allObjects = [
    {
      id: '1678903456254376289010',
      objectName: 'Text',
      description: 'description of test1',
      apiName: 'Test1',
      masterObject: 'Object 1',
      helpText: 'This is just ans test text',
      createdBy: 'Company Accounts',
      createDate: new Date(),
      updatedBy: 'Company Accounts',
      updateDate: new Date(),
      allFields: ['1678903888666444289010', '1865342387653445566778'],
      isStandard: true
    },
    {
      id: '18943200298354189523',
      objectName: 'Text2',
      description: 'description of test2',
      apiName: 'Test2',
      masterObject: 'Object 2',
      helpText: 'This is just ans test text2',
      createdBy: 'Company Accounts',
      createDate: new Date(),
      allFields: ['1678903888669988776655'],
      isStandard: false
    },
    {
      id: '1865342387653445566980',
      objectName: 'Text3',
      description: 'description of test3',
      apiName: 'Test3',
      masterObject: 'Object 3',
      helpText: 'This is just ans test text',
      createdBy: 'Company Accounts',
      createDate: new Date(),
      updatedBy: 'Company Accounts',
      updateDate: new Date(),
      allFields: [],
      isStandard: true
    },
    {
      id: '1865300099953445566980',
      objectName: 'Text4',
      description: 'description of test4',
      apiName: 'Test4',
      masterObject: 'Object 4',
      helpText: 'This is just ans test text',
      createdBy: 'Company Accounts',
      createDate: new Date(),
      updatedBy: 'Company Accounts',
      updateDate: new Date(),
      allFields: [],
      isStandard: false
    },
  ];

  constructor() { }

  generateId() {
    let text = '1';
    const possible = '0123456789';
    for (let i = 0; i < 21; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  getAllAdminObjects(): Observable<Objects[]> {
    return Observable.create( observer => {
      observer.next(this.allObjects);
      observer.complete();
    });
  }

  addAdminObject(obj): Observable<any> {
    this.allObjects.push(Object.assign({}, obj, {
      id: this.generateId(),
      createDate: new Date(),
      createdBy: 'Admin',
      masterObject: 'Object 3',
      allFields: [],
      isStandard: true
    }));
    return Observable.create( observer => {
      observer.next({done: true});
      observer.complete();
    });
  }

  editAdminObject(obj): Observable<any> {
    const ID = this.allObjects.findIndex(o => o.id === obj.id);
    this.allObjects[ID] = Object.assign({}, this.allObjects[ID], {
      updateDate: new Date(),
      updatedBy: 'Admin',
      objectName: obj.name,
      description: obj.description,
      apiName: obj.apiName,
      helpText: obj.apiName
    });
    return Observable.create( observer => {
      observer.next({done: true, obj: this.allObjects[ID]});
      observer.complete();
    });
  }

  deleteAdminObject(id): Observable<any> {
    this.allObjects.splice(this.allObjects.findIndex(obj => obj.id === id), 1);
    return Observable.create( observer => {
      observer.next({done: true, ID: id});
      observer.complete();
    });
  }

  getAllAdminObjectsFields(id): Observable<Fields[]> {
    const all = [];
    this.allObjects.find(obj => obj.id === id).allFields.forEach(FIELD => {
      all.push(this.allFields.find(field => field.id === FIELD));
    });
    return Observable.create( observer => {
      observer.next(all);
      observer.complete();
    });
  }

  addAdminObjectField(obj) {
    const fieldID = this.generateId();
    this.allFields.push(Object.assign({}, obj, {
      id: fieldID
    }));
    const ID = this.allObjects.findIndex(o => o.id === obj.refObject);
    this.allObjects[ID].allFields.push(fieldID);
    return Observable.create( observer => {
      observer.next({done: true});
      observer.complete();
    });
  }

  editAdminObjectField(obj) {
    const ID = this.allFields.findIndex(F => F.id === obj.id);
    const oldID = this.allFields[ID].refObject;
    const newID = obj.refObject;
    if (newID !== oldID) {
      const i = this.allObjects.findIndex(o => o.id === newID);
      this.allObjects[i].allFields.push(obj.id);
      const j = this.allObjects.findIndex(p => p.id === oldID);
      const k = this.allObjects[j].allFields.findIndex(q => q === this.allFields[ID].id);
      this.allObjects[j].allFields.splice(k, 1);
    }
    this.allFields[ID] = obj;
    return Observable.create( observer => {
      observer.next({done: true});
      observer.complete();
    });
  }

  deleteAdminObjectField(id, pID): Observable<any> {
    const ID = this.allObjects.findIndex(o => o.id === pID);
    const spiceID = this.allObjects[ID].allFields.findIndex(I => I === id);
    this.allObjects[ID].allFields.splice(spiceID, 1);
    return Observable.create( observer => {
      observer.next({done: true, ID: id});
      observer.complete();
    });
  }

}

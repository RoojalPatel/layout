export class Field {
    id?: number;
    name: string;
    system: boolean;
    active: boolean;
    objectId: number;
    createDate: string;
    createdBy: number;
    updateDate: string;
    updatedBy: number;
    createdByDescription: string;
    updatedByDescription: string;

    description: string;
}
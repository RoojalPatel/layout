
export interface Objects {
  createDate?: string;
  createdBy?: number;
  updateDate?: string;
  updatedBy?: number;
  id?: number;
  masterObject: number;
  objectName: string;
  apiName: string;
  description: string;
  helpText: string;
  nameField: string[];
  isStandard:	boolean;
  objectCode:	string;
  createdByDescription:	string;
  updatedByDescription:	string;
}

export interface Fields {
  id: number;
  objectId: number;
  name: string;
  key: string;
  description: string;
  help: string;
  apiName: string;
  mandatory: boolean;
  secure: boolean;
  unique: boolean;
  defaultValue: string;
  custom: boolean;
  type: string;
  length: number;
  decimals: number;
  autogen: boolean;
  sequenceName: string;
  format: string;
  lov: string;
  formula: string;
  refObject: number;
  refObjectName: string;
  basicSearch: boolean;
  advancedSearch: boolean;
  searchType: string;
  createDate?: string;
  createdBy?: number;
  updateDate?: string;
  updatedBy?: number;
  createdByDescription:	string;
  updatedByDescription:	string;
}

import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { ObjectName } from "../../admin/models/objectname.model";


@Injectable({
  providedIn:'root'
})

export class Breadcrumbservice{
  breadCrumbDataModel;
  breadCrumbTreeDataModel: any[];

  public generateBreadcrumbItem(object: any){
    return {elevalue: object, label: object.name};
  }

  public regenerateBreadCrumbOfChild(childs: any[]){
    setTimeout(() => {
      //Remove all bread crum item from array except first item
      this.breadCrumbTreeDataModel.splice(1);
      
      if(childs){
        childs.forEach(child => {
          this.breadCrumbTreeDataModel.splice(1, 0, this.generateBreadcrumbItem(child));
        });
      }
    });
  }
}
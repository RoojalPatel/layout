import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { LoginService } from './../business/login.service';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { tokenNotExpired } from 'angular2-jwt';

import { Routes, Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
     private router:Router,
     private messageServ:MessageService
    ) {

    }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    let token = localStorage.getItem('id_token')!=undefined?
    localStorage.getItem('id_token'):
    sessionStorage.getItem("id_token");
    
    if(token != null && token!=""){
        request = request.clone({
        setHeaders: {
            "X-Authorization": `Bearer ${token}`
        }
        });
    }
    
    return next.handle(request).pipe(
      map(
        (event:HttpEvent<any>)=>{
          return event;
        }
      ),
      catchError(
        
        (error:any, caught)=>{
          if(error instanceof HttpErrorResponse){
            let message = {
              severity:"error",
              summary:error.statusText,
              detail:""
              };
            
            if(error.error != undefined){
              message.detail =  error.error.message;
              if(error.error.errorCode == 11){
                this.router.navigate(["login"]);
              }
            }
            this.messageServ.add(message);
          }
          
          return Observable.throw(error);
        }
      )
      );
  }


}
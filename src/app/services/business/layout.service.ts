import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn:"root"
})
export class LayoutService {
  private url = "http://localhost:3000/";
  constructor(
    private http:HttpClient
  ) { }

  getAllFields():Observable<any>{
    return this.http.get(this.url+"fields");
  }

  getFieldById(fieldId):Observable<any>{
    return this.http.get(this.url+"fields?id="+fieldId);
  }

  getAllRelatedItems():Observable<any>{
    return this.http.get(this.url+"relatedItems");
  }

  insertLayout(layout):Observable<any>{
    return this.http.post(this.url+"layouts", layout, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  getAllLayouts(objectId):Observable<any>{
    return this.http.get(this.url+"layouts?object_id="+objectId);
  }

  getAllObjectRecordLayouts(objectId):Observable<any>{
    return this.http.get(this.url+"layouts?object_id="+objectId+"&layout_type=object_record");
  }

  getBasicSearchLayout(objectId):Observable<any>{
    return this.http.get(this.url+"layouts?object_id="+objectId+"&layout_type=basic_search");
  }
  
  getAdvancedSearchLayout(objectId):Observable<any>{
    return this.http.get(this.url+"layouts?object_id="+objectId+"&layout_type=advanced_search");
  }
  
  getSearchResultLayout(objectId):Observable<any>{
    return this.http.get(this.url+"layouts?object_id="+objectId+"&layout_type=search_result");
  }

  getLayoutById(layoutId):Observable<any>{
    return this.http.get(this.url+"layouts/"+layoutId);
  }

  updateLayout(layoutId, payload):Observable<any>{
    return this.http.put(this.url+"layouts/"+layoutId, payload, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    }); 
  }

  deleteLayout(layoutId):Observable<any>{
    return this.http.delete(this.url+"layouts/"+layoutId);
  }

}

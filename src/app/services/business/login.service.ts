import { LoggedUser } from './../../models/logged-user.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse  } from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';



@Injectable({
  providedIn:"root"
})
export class LoginService {
  private isAuthenticated:boolean = false;
  private userDetails;
  private rememberMe:boolean;
  private token:string;
  public loginLoading = false;
  
  constructor(
    private http:HttpClient,
    private router:Router
  ) { 
    this.token = sessionStorage.getItem("id_token")!=undefined?
    sessionStorage.getItem("id_token"):localStorage.getItem("id_token");
  }

  doLogin(uName:string, password:string, tenantCode:string, rembMe:boolean=false):void{
    this.loginLoading = true;
    this.rememberMe = rembMe;
    let data = {
      userName:uName,
      password:password,
      tenantCode: tenantCode
    }
    let url = "/api/auth/login";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      })
    };
    this.http.post<LoggedUser>(url, JSON.stringify(data), httpOptions).subscribe(
      (user:LoggedUser)=>{
        this.token = user.token;
        this.userDetails = user;
        localStorage.setItem("user_details", JSON.stringify(this.userDetails));
        this.storeToken(user.token);
        this.loginLoading = false;
        this.router.navigate(['/home/dashboard']);
      },
      (error:HttpErrorResponse)=>{
        this.loginLoading = false;
      }
    );
  }

  storeToken(token){
    if(this.rememberMe)
      localStorage.setItem("id_token", token);
    else
      sessionStorage.setItem("id_token", token);
  }

  loginWithToken(){
    let token = localStorage.getItem("id_token")!=""? localStorage.getItem("id_token"):sessionStorage.getItem("id_token");
    if(tokenNotExpired("", token)){
      this.token = token;
      this.userDetails = this.getUserDetails();
      return true;
    }
    else{
      return false;
    }
  }

  checkLogin():boolean{
    return tokenNotExpired("", this.token)?true:false;
  }

  setUserDetails(){
    this.userDetails = JSON.parse(localStorage.getItem("user_details"));
  }

  getUserDetails(){
    this.userDetails = JSON.parse(localStorage.getItem("user_details"));
    return this.userDetails;
  }

  doLogout(){
      sessionStorage.removeItem("id_token");  
      localStorage.removeItem("id_token");   
      localStorage.removeItem("user_details");  
      this.userDetails = null;
  }

  getToken(){
    return localStorage.getItem("id_token");
  }
}

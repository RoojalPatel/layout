import { LoginService } from './login.service';
import { Injectable } from '@angular/core';
import { CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot } from '@angular/router';
import { Routes, Router } from '@angular/router';

@Injectable({
  providedIn:"root"
})
export class RouteguardService implements CanActivate {

  constructor(private auth:LoginService, private router:Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
    if(this.auth.checkLogin()){
      this.auth.setUserDetails(); 
      return true;
    }
    else
    {
      this.router.navigate(["/login"]);
      return false;
    }
      
  }
}


import { LayoutService } from './layout.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn:"root"
})
export class ScreensService {
  private url = "http://localhost:3000/";

  constructor(
    private http:HttpClient,
    private layoutServ:LayoutService
  ) { 

  }

  saveScreen(screen):Observable<any>{
    return this.http.post(this.url+"screens", screen, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  getAllScreens(objectid):Observable<any>{
    return this.http.get(this.url+"screens?object_id="+objectid);
  }

  getScreenById(screenId):Observable<any>{
    return this.http.get(this.url+"screens/"+screenId);
  }

  deleteScreen(screenId):Observable<any>{
    return this.http.delete(this.url+"screens/"+screenId);
  }

  updateScreen(screenId, screenData):Observable<any>{
    return this.http.put(this.url+"screens/"+screenId, screenData, {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
  }

  getRelatedItemsvalues(relatedItemObjectId):Observable<any>{
    return this.http.get(this.url+"relatedItemsScreen/?objectId="+relatedItemObjectId);
  }
  
}

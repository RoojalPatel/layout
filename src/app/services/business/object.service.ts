import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn:"root"
})
export class ObjectService {
  private url = "http://localhost:3000/objects/";
  constructor(
    private http:HttpClient
  ) { }

  getAllObjects():Observable<any>{
    return this.http.get(this.url);
  }

  getObjectById(objectId):Observable<any>{
    return this.http.get(this.url+objectId);
  }

}

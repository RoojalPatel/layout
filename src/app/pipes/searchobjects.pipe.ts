import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchobjects'
})
export class SearchobjectsPipe implements PipeTransform {

  transform(items: any[], searchText: string, fieldname?: string[]): any[] {
    if(!items) return [];
    if(searchText == null || searchText=="") return items;
    searchText = searchText.toLowerCase();

    return items.filter(element=>{
      if(fieldname){
        let flag = false;
        fieldname.forEach(field => {
            if(element[field].toLowerCase().includes(searchText)){
              flag = true;
            }
        });
        return flag;
      }else{
        return element.name.toLowerCase().includes(searchText);
      }
    });   
   }

}

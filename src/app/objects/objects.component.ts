import { ObjectService } from './../services/business/object.service';
import { ActivatedRoute } from '@angular/router';
import { LayoutService } from './../services/business/layout.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ScreensService } from '../services/business/screens.service';
import {MenuItem} from 'primeng/api';
import { SlideInOutAnimation } from './../app.animations';
import { Breadcrumbservice } from '../services/utility/bread-crumb.service';

@Component({
  selector: 'app-objects',
  animations: SlideInOutAnimation,
  templateUrl: './objects.component.html',
  styleUrls: ['./objects.component.css']
})
export class ObjectsComponent implements OnInit {
  public screens = [];
  objectId;
  object;
  items:MenuItem[];
  items1:MenuItem[];
  screen = null;
  layout = null;
  basicSearchLayout;
  searchResultLayout;
  rows;
  columns;
  data;
  objectMenuItems:MenuItem[];
  searchResultLayoutColumns = [];
  searchResults;

  constructor(
    private route: ActivatedRoute,
    private layoutServ:LayoutService,
    private router:Router,
    private screenServ:ScreensService,
    private objectService:ObjectService,
    private breadCrumbServ:Breadcrumbservice
  ) { 

  }

  ngOnInit() {
    
    this.route.params.subscribe( params => {
      this.layout = null;
      this.screen = null;
      
      this.objectId = params.objectID;
      
      this.objectService.getObjectById(this.objectId).subscribe(
        data=>{
          this.object = data;
          this.breadCrumbServ.breadCrumbDataModel = [
            {label: this.object.name, routerLink:['/home/objects', this.objectId]}
          ];
        }
      );

      this.layoutServ.getBasicSearchLayout(this.objectId).subscribe(
        data=>{
          this.basicSearchLayout = data[0];
          if(this.basicSearchLayout !=undefined){
            this.basicSearchLayout.mainSection[0].columns[0].columnData.forEach(element => {
              element.fieldLovs = [];
              this.formatLayoutFields(element);
              this.populateFieldElementType(element);
            });
          }
          
        }
      );

      this.layoutServ.getSearchResultLayout(this.objectId).subscribe(
        data=>{
          this.searchResultLayout = null;
          this.screens = [];
          this.searchResultLayoutColumns = [];

          this.searchResultLayout = data[0];
          if(this.searchResultLayout != undefined){
            this.searchResultLayout.mainSection[0].columns[0].columnData.forEach(element => {
              element.fieldLovs = [];
              element.value="";
              this.formatLayoutFields(element);
              this.searchResultLayoutColumns.push({"header":element.fieldName, "field":element.fieldId});
              this.populateFieldElementType(element);
            });
          }
          this.screenServ.getAllScreens(this.objectId).subscribe(
            data=>{
              let tempData = data;
              tempData.forEach(screen=>{
                let fieldValObj = {};
                fieldValObj['screenId'] = screen.id;
                screen.data.forEach(element => {
                  fieldValObj[element.fieldId] = element.fieldValue;                
                });
                this.screens.push(fieldValObj);
              });
          });
        }
      );
      this.items = [
        {label: 'Delete', icon: 'fa-trash', command: () => {
          this.goToCreateNew();
        }}
      ];
      this.items1 = [
        {label: 'Delete', icon: 'fa-trash', command: () => {
          //this.deleteScreen(this.screen);
        }}
      ];
    });       
  }

  formatLayoutFields(field){
    this.layoutServ.getFieldById(field.fieldId).subscribe(
      data=>{
        let tempData=data[0];
        field.fieldName = tempData.field_name;
        field.fieldType = tempData.field_type;
        
        field.fieldLovs = tempData.field_lov!=undefined?tempData.field_lov:[];
      }
    );
  }

  goToAdvanceSearch(){
    this.router.navigate(['/home/advance-search', this.objectId]);
  }

  goToOtherDetails(){
    this.router.navigate(['/home/other-details', this.objectId]);
  }

  doSearch(){

  }

  populateFieldElementType(data){
    switch(data.fieldType){
      case "String":
        data.fieldElementType = "text";
        break;
      case "Number":
        data.fieldElementType = "number";
        break;
      case "Boolean":
        data.fieldElementType = "radio";
        break;
      case "Date":
        data.fieldElementType = "date";
        break;

    }

    if(data.fieldLovs.length>0){
      data.fieldElementType = "dropdown";
    }
  }

  goToCreateNew(){
    this.router.navigate(['/home/create-screen', this.objectId]);
  }

  goToEdit(screenId){
    this.router.navigate(['/home/edit-screen', this.objectId, screenId]);
  }

  loadDetails(id){
    this.router.navigate(['/home/view-screen', this.objectId, id]);
  }
}

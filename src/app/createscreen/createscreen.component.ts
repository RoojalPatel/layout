import { ObjectService } from './../services/business/object.service';
import { ActivatedRoute,Router } from '@angular/router';
import { ScreensService } from './../services/business/screens.service';
import { DragNDropDataModel } from './../createlayout/model/dragndropdata.model';
import { LayoutService } from './../services/business/layout.service';
import { ScreenModel } from './model/screen.model';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectItem, MenuItem } from 'primeng/api';
import { Breadcrumbservice } from '../services/utility/bread-crumb.service';

@Component({
  selector: 'app-createscreen',
  templateUrl: './createscreen.component.html',
  styleUrls: ['./createscreen.component.css']
})
export class CreatescreenComponent implements OnInit {
  public screen:ScreenModel = new ScreenModel;
  public layout = new DragNDropDataModel;
  public form:FormGroup = new FormGroup({});
  public formControls= {};
  public screenId="";
  public objectId;
  createDialogDisplay = false;
  allLayouts: SelectItem[] = [];

  constructor(
    private layoutServ:LayoutService,
    private location:Location,
    private objectService:ObjectService,
    private screenServ:ScreensService,
    private route: ActivatedRoute,
    private router:Router,
    private breadCrumbServ:Breadcrumbservice
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.objectId = params.objectID;
      this.objectService.getObjectById(this.objectId).subscribe(
        data=>{
          let obj = data;
          this.breadCrumbServ.breadCrumbDataModel = [
            {label:obj.name, routerLink:['/home/objects', obj.id]}
          ];

          if(params.screenId != undefined){
            this.breadCrumbServ.breadCrumbDataModel.push({label:'Edit '+obj.name});
          }
          else{
            this.breadCrumbServ.breadCrumbDataModel.push({label:'Create '+obj.name});
          }
        }
      );
      
      if(params.screenId != undefined){
        this.screenId = params.screenId;
        this.screenServ.getScreenById(params.screenId).subscribe(
          data=>{
            this.screen = data;
            this.doDataPopulation();    
          }
        );
      }
      else{
        this.layoutServ.getAllObjectRecordLayouts(this.objectId).subscribe(
          data => {
            let tempData = data;
            tempData.forEach(element => {
              this.allLayouts.push({
                label:element.layoutName, value:element.id
              });
            });
          }
        );
        this.createDialogDisplay = true;
      }
    });
  }

  goBack(){
    this.location.back();
  }

  saveScreenDetails(screen, layout){
    this.createDialogDisplay = false;
    this.screen.screenName = screen;
    this.screen.layoutId = layout;
    this.screen.object_id = this.objectId;
    this.doDataPopulation();  
  }

  doDataPopulation(){
    this.layoutServ.getAllFields().subscribe(
      fields=>{
        let allFields = fields;
        this.layoutServ.getLayoutById(this.screen.layoutId).subscribe(
          data=>{
            let tempData = data;
            tempData.mainSection = tempData.mainSection.filter(sect=>{
              return sect.sectionContent != "related_items"; 
            });
            tempData.mainSection.forEach(section=>{
              section.columns.forEach(column=>{
                column.columnData.forEach(fieldData=>{
                  let originalField = allFields.filter((fieldElement)=>{
                    return fieldElement.id == fieldData.fieldId;
                  })[0];
                  fieldData.fieldName   = originalField.field_name;
                  fieldData.fieldType   = originalField.field_type;
                  fieldData.fieldLength = originalField.field_length;
                  fieldData.fieldLovs   = originalField.field_lov?originalField.field_lov:[];
                  fieldData.options = [];
                  fieldData.fieldLovs.forEach(element => {
                    fieldData.options.push({
                      label:element, value:element
                    });
                  });

                  this.populateFieldElementType(fieldData);
                  let screenData = this.screen.data.filter(element=>{
                    return fieldData.fieldId == element.fieldId;
                  })[0];
                  let initialDataValue = "";
                  if(screenData != undefined){
                    initialDataValue = screenData.fieldValue;
                  }
                  if(!fieldData.mandatory){

                    this.formControls[fieldData.fieldName]=
                      new FormControl(initialDataValue);
                  }
                  else{
                    this.formControls[fieldData.fieldName]=
                      new FormControl(initialDataValue, [
                        Validators.required]
                      );
                  }
                });
              });
            });
            this.layout = tempData;
            this.form = new FormGroup(this.formControls);

        });
    });
  }

  saveScreen(){
    if(this.form.invalid)
      return false;
    
    if(this.screenId){
      let tempData = this.layout;
      let ids = [];
      tempData.mainSection.forEach(section=>{
        section.columns.forEach(column=>{
          column.columnData.forEach(fieldData=>{
            ids.push(fieldData.fieldId);
            let screenData = this.screen.data.filter(element=>{
              return fieldData.fieldId == element.fieldId;
            })[0];
            if(screenData != undefined)
              screenData.fieldValue = this.formControls[fieldData.fieldName].value;
            else{
              let data = {
                fieldId:fieldData.fieldId,
                fieldValue:this.formControls[fieldData.fieldName].value
              };
              this.screen.data.push(data);            
            }

          });
        });
      });
      let newDataSet = new Set(ids);
      this.screen.data = this.screen.data.filter(screenData=>{
        return newDataSet.has(screenData.fieldId);
      });
      this.screenServ.updateScreen(this.screenId, this.screen).subscribe(
        data=>{
          this.location.back();
        }
      );
    }
    else{
      let tempData = this.layout;
      tempData.mainSection.forEach(section=>{
        section.columns.forEach(column=>{
          column.columnData.forEach(fieldData=>{
            let data = {
              fieldId:fieldData.fieldId,
              fieldValue:this.formControls[fieldData.fieldName].value
            };
            this.screen.data.push(data);
          });
        });
      });
      this.screenServ.saveScreen(this.screen).subscribe(
        data=>{
          this.location.back();
        }
      );
    }
    
  }

  populateFieldElementType(data){
    switch(data.fieldType){
      case "String":
        data.fieldElementType = "text";
        break;
      case "Number":
        data.fieldElementType = "number";
        break;
      case "Boolean":
        data.fieldElementType = "radio";
        break;
      case "Date":
        data.fieldElementType = "date";
        break;

    }

    if(data.fieldLovs.length>0){
      data.fieldElementType = "dropdown";
    }
  }

}

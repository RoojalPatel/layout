import { ObjectService } from './../services/business/object.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from '@angular/common';
import { LayoutService } from '../services/business/layout.service';
import { Breadcrumbservice } from '../services/utility/bread-crumb.service';

@Component({
  selector: 'app-pagelayouts',
  templateUrl: './pagelayouts.component.html',
  styleUrls: ['./pagelayouts.component.css']
})
export class PagelayoutsComponent implements OnInit {
  public objectId;
  public layouts = [];
  public object;
  mainLayout;
  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private location:Location,
    private layoutService:LayoutService,
    private objectService:ObjectService,
    private breadCrumbServ:Breadcrumbservice
  ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.objectId = params.objectID;
      this.layoutService.getAllLayouts(this.objectId).subscribe(
        data=>{
          this.layouts = data;
        }
      );
      this.objectService.getObjectById(this.objectId).subscribe(
        data=>{
          this.object=data;
          this.breadCrumbServ.breadCrumbDataModel = [
            {label:this.object.name, routerLink:['/home/objects', this.object.id]},
            {label:'Layouts'}
          ];
        }
      );
    });
  }

  goToCreateNew(){
    this.router.navigate(["home/create-layout", this.objectId]);
  }

  viewLayout(layoutId, name){
    this.layoutService.getAllFields().subscribe(
      data=>{
        let fieldData = data;
        this.layoutService.getLayoutById(layoutId).subscribe(
          data=>{
            let tempData = data;
            tempData.mainSection.forEach(section=>{
              section.columns.forEach(column=>{
                column.columnData.forEach(data=>{
                  let originalField = fieldData.filter((fieldElement)=>{
                    //console.log(fieldElement);
                    return fieldElement.id == data.fieldId;
                  })[0];
                  data.fieldName = originalField.field_name;
                  data.fieldType = originalField.field_type;
                });
              });
            });
            this.mainLayout = tempData;
          }
        );        
      }
    );
  }

  editLayout(layout){    
    this.router.navigate(["home/edit-layout", this.objectId, layout.id]);
  }

  deleteLayout(layout){
    this.layoutService.deleteLayout(layout.id).subscribe(
      data=>{
        this.layouts = this.layouts.filter(element=>{
          return layout.id != element.id;
        });
        this.mainLayout = "";
      }
    );
  }
}

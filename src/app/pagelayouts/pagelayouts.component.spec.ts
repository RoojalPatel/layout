import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagelayoutsComponent } from './pagelayouts.component';

describe('PagelayoutsComponent', () => {
  let component: PagelayoutsComponent;
  let fixture: ComponentFixture<PagelayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagelayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagelayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

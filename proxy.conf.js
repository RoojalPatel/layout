var HttpsProxyAgent = require('https-proxy-agent');
var proxyConfig = [{
  context: '/api',
  // target: "http://34.216.142.69:9001",
  target:"http://testdevbox.stacksoft.com:9001",
  secure: false,
  changeOrigin: true
}];

function setupForCorporateProxy(proxyConfig) {
  var proxyServer = "http://proxy-slt.lbc.tdk.dk:8080";
  if (proxyServer) {
    var agent = new HttpsProxyAgent(proxyServer);
    console.log('Using corporate proxy server: ' + proxyServer);
    proxyConfig.forEach(function(entry) {
      entry.agent = agent;
    });
  }
  return proxyConfig;
}

module.exports = setupForCorporateProxy(proxyConfig);
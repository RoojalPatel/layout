# Dynamiclayout

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.4.

## Code-base set up

Install `node` latest version.

clone the repository, checkout appropriate branch.

Then run `npm install -g json-server` to install the local DB, use `sudo` for MAC.

Then run `npm install -g @angular/cli` to install angular CLI, use `sudo` for MAC.

Then navigate to cloned project directory, run `npm install`. it will install all dependencies and will take a while to install all.

Now you are good to .. 

## Development server

Run `json-server --watch db.json`, it will start the JSON data base at `http://localhost:3000/`.

Then Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
